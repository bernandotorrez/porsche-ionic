import { Component } from '@angular/core';
import { LoginService } from '../login/login.service';
import { Login } from '../login/login.model';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private loading: boolean = false;
  private loginData: Login;
  private optionSlide = {
    speed: 900,
    centeredSlides: true,
    loop: true,
    observer: true,
    observeParents: true,
    spaceBetween: 100,
    initialSlide: 1,
    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
  }

  constructor(
    private loginService: LoginService,
    private apiService: ApiService
  ) {
    
  }

  ionViewWillEnter() {
    this.loginData = this.loginService.getLoginData();
    console.log(this.apiService.getAuthToken);
  }

}
