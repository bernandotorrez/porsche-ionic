import { Component, OnInit } from '@angular/core';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Location } from './location.model';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-dealer',
  templateUrl: './dealer.page.html',
  styleUrls: ['./dealer.page.scss'],
})
export class DealerPage implements OnInit {

  private latitude: number;
  private longitude: number;
  private loadMap: boolean = false;
  private locationPermission: boolean = false;

  locationWatchStarted:boolean;
  locationSubscription:any;
 
  locationTraces = [];

  constructor(
    private geolocation: Geolocation,
    private androidPermissions: AndroidPermissions,
    private toastController: ToastController
  ) { 
    androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
      result => {
        if(result.hasPermission == false) {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)
        } else {
          this.locationPermission = true;
          this.showMap();
        }
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)
    );
  }

  ngOnInit() {
  }

  async showMap() {
     this.loadMap = true;

     this.geolocation.getCurrentPosition().then((resp) => {
      let {latitude, longitude} = resp.coords;

       this.latitude = latitude;
       this.longitude = longitude;
     }).catch((error) => {
       console.log('Error getting location', error);
     });

     let watch = await this.geolocation.watchPosition();
     watch.subscribe((data) => {
       let {latitude, longitude} = data.coords;

       this.latitude = latitude;
       this.longitude = longitude;
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
     });
  }

  async toast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: 'success'
    });

    toast.present()
  }

}
