import { Component, OnInit } from '@angular/core';
import { Login } from '../login/login.model';
import { LoginService } from '../login/login.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage {

  private loginData: Login;

  constructor(
    private loginService: LoginService
  ) { }

  ngOnInit() {
    this.loginData = this.loginService.getLoginData();
  }

  ionViewWillEnter() {
    
  }

}
