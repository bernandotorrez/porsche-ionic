import { Component, OnInit } from '@angular/core';
import { Login } from '../login/login.model';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';
import { ToastController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  private loginData: Login

  constructor(
    private loginService: LoginService,
    private router: Router,
    private toastController: ToastController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.loginData = this.loginService.getLoginData();
  }

  async doLogout() {
    const alert = await this.alertController.create({
      header: 'Confirmation',
      message: 'Are you sure to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            
          }
        }, {
          text: 'Sure!',
          handler: () => {
            this.onLogOut()
          }
        }
      ]
    });

    await alert.present();
  }

  async onLogOut() {
    this.loginService.doLogOut();
    const toast = await this.toast(`Logged Out!`);
    toast.present();
    this.router.navigate(['/home']);
  }

  toast(message: string) {
    const toast = this.toastController.create({
      message: message,
      duration: 2000,
      color: 'success'
    });

    return toast;
  }

}
