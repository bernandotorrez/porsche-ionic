import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  public api_url: string = 'https://192.168.100.7:3001';
  public _authToken: string = '';

  constructor(private http: HttpClient) { }

  set setAuthToken(token: string) {
    this._authToken = token;
  }

  get getAuthToken() {
    return this._authToken;
  }

}
