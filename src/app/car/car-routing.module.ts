import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CarPage } from './car.page';

const routes: Routes = [
  {
    path: '',
    component: CarPage
  },
  {
    path: 'car-type/:id_type/:desc_model',
    loadChildren: () => import('./car-type/car-type.module').then( m => m.CarTypePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CarPageRoutingModule {}
