import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CarTypeService } from './car-type.service';
import { CarType } from './car-type.model';

@Component({
  selector: 'app-car-type',
  templateUrl: './car-type.page.html',
  styleUrls: ['./car-type.page.scss'],
})
export class CarTypePage implements OnInit {

  private carTitle: String;
  private ModelID: string;
  private carType: CarType[];
  private foundCar: CarType[];
  private loading: boolean = true;

  constructor(
    private activedRoute: ActivatedRoute,
    private carTypeService: CarTypeService
    ) { }

  ngOnInit() {
    this.activedRoute.paramMap.subscribe(paramMap => {
      if(!paramMap.has('id_type')) {
        return;
      }

      const id_type = paramMap.get('id_type');
      this.ModelID = id_type;

      if(!paramMap.has('desc_model')) {
        return;
      }

      const desc_model = paramMap.get('desc_model');
      this.carTitle = desc_model;
    });

  }

  async ionViewDidEnter() {
    this.carType = await this.carTypeService.ngOnInit(this.ModelID);
    this.loading = false;   
  }

  async searchCarType(search: string) {
    if(search.length == 0) {
      this.carType = await this.carTypeService.getAllCarType();
    } else {
      this.foundCar = this.carTypeService.searchCarType(search);
      this.carType = this.foundCar;
    }
  }

}
