export interface CarType {
    id_type: Number;
    id_model: Number;
    desc_type: String;
    price_type: Number;
    status_del: Number;
    desc_model: String;
}