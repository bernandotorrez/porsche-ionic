import { Injectable } from '@angular/core';
import { CarType } from './car-type.model';
import { ApiService } from 'src/app/api.service';

@Injectable({
  providedIn: 'root'
})
export class CarTypeService {

  //private api_url: string = 'https://localhost:3001';
  private api_url: string;
  private carType: CarType[];
  private auth_token: string;
  
  constructor(
    private apiService: ApiService
  ) { 
    this.api_url = apiService.api_url;
  }
  
  async ngOnInit(ModelID: string) {
    this.auth_token = this.apiService.getAuthToken;
    const header = {
      'X-Auth-Token': this.auth_token
    }

    const response = await fetch(`${this.api_url}/car/all/model/type?id=${ModelID}`, {
      method: 'GET',
      headers: header
    });
    
    const result = await response.json();
    const car_data = result.data;
    this.carType = car_data;

    return[...this.carType];
  }

  getAllCarType() {
    return[...this.carType];
  }

  searchCarType(search: string) {
    let searchCar = this.carType.filter(car => car.desc_type.toLowerCase().includes(search.toLowerCase()));
 
    return searchCar;    
  }
}
