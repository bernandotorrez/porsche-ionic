import { Injectable } from '@angular/core';
import { Car } from './car.model';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  private api_url: string;
  private cars: Car[];
  private auth_token: string;

  constructor(
    private apiService: ApiService
  ) { 
    this.api_url = apiService.api_url;
    this.ngOnInit();
  }

  getAllCarModel() {
    return [...this.cars];
  }

  async ngOnInit() {
    this.auth_token = this.apiService.getAuthToken;
    const header = {
      'X-Auth-Token': this.auth_token
    }

    const response = await fetch(`${this.api_url}/car/all/model`, {
      method: 'GET',
      headers: header
    });
    
    const result = await response.json();
    const car_data = result.data;
    this.cars = car_data;
  }

  searchCarModel(search: string) {
    let searchCar = this.cars.filter(car => car.desc_model.toLowerCase().includes(search.toLowerCase()));
 
    return searchCar;    
  }
}
