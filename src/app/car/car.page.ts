import { Component, OnInit } from '@angular/core';
import { Car } from './car.model';
import { CarService } from './car.service';

@Component({
  selector: 'app-car',
  templateUrl: './car.page.html',
  styleUrls: ['./car.page.scss'],
})
export class CarPage {

  //private cars: String[] = [];
  cars: Car[];
  foundCar: Car[];
  private loading: boolean = true;

  constructor(private carService: CarService) { }

  ionViewDidEnter() {
    this.cars = this.carService.getAllCarModel();
    this.loading = false;
  }

  searchCarModel(search: string) {
    if(search.length == 0) {
      this.cars = this.carService.getAllCarModel();
    } else {
      this.foundCar = this.carService.searchCarModel(search);
      this.cars = this.foundCar;
    }
  }

}
