import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Login } from './login.model';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private loginData: Login;
  loginForm: FormGroup;
  isSubmitted: boolean = false;

  constructor(
    private loginService: LoginService,
    private router: Router,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      password: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]]
    })
  }

  submitLoginForm() {
    this.isSubmitted = true;
    if (!this.loginForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      const {email, password} = this.loginForm.value;
      this.doLogin(email, password);
    }
  }

  get errorControl() {
    return this.loginForm.controls;
  }

  async doLogin(email: string, password: string) {
    const loading = await this.presentLoading();
    
    loading.present();
    this.loginData = await this.loginService.doLogin(email, password);
    loading.dismiss();

    if(this.loginData == null) {
      const message = 'Email or Password is Wrong!';
      const toast = await this.toast(message);
      toast.present();
    } else {
      this.router.navigate(['/home']);
    }
    
  }

  presentLoading() {
    const loading = this.loadingController.create({
      message: 'Logging In ...'
    })
      return loading;
  }

  toast(message: string) {
    const toast = this.toastController.create({
      message: message,
      duration: 2000,
      color: 'primary'
    });

    return toast;
  }

}
