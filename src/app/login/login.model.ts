export interface Login {
    email: string;
    name: string;
    address: string;
    phone: string;
    gender: string;
    date_birth: Date;
    verification: string;
    api_token: string;
    status: number;
}