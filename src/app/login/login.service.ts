import { Injectable } from '@angular/core';
import { Login } from './login.model';
import { ApiService } from '../api.service';
import md5 from 'md5';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private api_url: string;
  private loginData: Login;

  constructor(
    private apiService: ApiService
  ) { 
    this.api_url = apiService.api_url
  }

  ngOnInit() {
   
  }

  async doLogin(email: string, password: string) {
    const data = {
      email: email,
      password: md5(password)
    };

    const url = `${this.api_url}/login/doLogin`;

    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });

    const result = await response.json();

    const login_data = result.data;
    this.loginData = login_data;

    this.apiService.setAuthToken = response.headers.get('X-Auth-Token');
    
    return this.loginData;
  }

  getLoginData() {
    return this.loginData;
  }

  doLogOut() {
    this.loginData = null;
  }
  
}
